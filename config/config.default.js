/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_pluto-1';

  // 不需要token鉴权的白名单
  config.tokenWhiteList = [
    '/api/user/login',
    '/api/user/register',
    '/api/upload',
  ];

  // add your middleware config here
  // 加载 errorHandler 中间件
  // 中间件的顺序上，因为egg采用的是koa的洋葱模型，命中请求时，会按中间件的注册顺序依次走，当到达请求后，会逆着中间件注册的顺便反着走，即一个中间件会走两次，分别是next前后
  // 当我们把全局的错误中间件注册在中间件最末尾时，controller和service里面不使用try catch捕捉错误时，controller和sercive的错误就会被全局错误中间件捕捉
  // 综上，全局错误中间件需要注册在最后面
  config.middleware = ['jwtPars','errorHandler'];
  // 只对 /api 前缀的 url 路径生效
  config.errorHandler = {
    match: '/api',
  }
  // 对注册，登录以外的接口，进行token的解析
  // 以/api/开头的请求，且不在白名单内的请求组，就进行token解析
  config.jwtPars = {
    match(ctx) {
        const { request } = ctx;
        const { url } = request;
        const result = !config.tokenWhiteList.includes(url) && /^\/api\//.test(url);
        return result;
    }
  }

  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true
    },
    domainWhiteList: [ '*' ], // 配置白名单
  };

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  // mysql配置
  config.mysql = {
    client: {
	host: '127.0.0.1',
	port: '3306', // 端口
        user: 'root',
        password: '123456', // 123456 本地
        database: 'egg_bill',
    },
    // 是否加载到app上，默认开启
    app: true,
    // 是否加载到agent上,默认关闭
    agent: false,
  };

  // validate，参数校验配置
  config.validate = {
    convert: true, // 对参数是否使用convertType规则进行类型转换
    validateRoot: false, // 限制被验证值必须是一个对象
    widelyUndefined: true, // 把空字符串，NaN,null 这些转成 undefined，将这些异常的数据进行了统一，方便后续处理。
  }

  // 自定义jwt加密字符串
  config.jwt = {
    // secret 加密字符串，将在后续用于结合用户信息生成一串 token, secret 是放在服务端代码中,要防止泄漏
    secret: 'pluto',
  }

  // egg 提供两种文件接收模式，1 是 file 直接读取，2 是 stream 流的方式
  config.multipart = {
    mode: 'file'
  };

  // 图片保存的路径
  config.uploadDir = 'app/public/upload/image'

  // 解决跨域
  config.cors = {
    origin: '*', // 允许所有跨域访问
    credentials: true, // 允许 Cookie 跨域跨域
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  };

  return {
    ...config,
    ...userConfig,
  };
};
