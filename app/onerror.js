// 覆盖egg-onerror插件，进行自定义处理
module.exports = (app) => {
    return {
        '404': {},
        '403': {},
        '4xx': {
            * all(ctx, err) {
                // all不区分accepts，由开发者自行处理
            }
        },
        '500': {},
    }
}
