const PublicController = require('./public');

const { add_bill, delete_bill, listQuery } = require('../validate/bill');

const { add } = require('../util/math');

const format = require('../util/TimeFormats');
class BillController extends PublicController {
    /**
     * 新增账单
     */
    async add() {
        const { ctx, app } = this;
        // 获取用户id
        const { id: user_id } = ctx.request.userInfo;
        const body = ctx.request.body;
        // 校验入参
        ctx.validate(add_bill);
        let { pay_type, amount, type_id, remark = '', createdTime } = body;
        // 获取账单标签名称
        const { name: type_name, url: type_url } = await ctx.service.type.getDetails(type_id);
        // const createdTime = format('YYYY-MM-DD', this.timeStamp());
        createdTime = format('YYYY-MM-DD', createdTime);
        // 存储
        await ctx.service.bill.add({ pay_type, amount, type_id, user_id, type_name, type_url, remark, createdTime, active: '1' });
        this.successful();
    }

    // 编辑账单
    async update() {
        const { ctx, app } = this;
        const { id: user_id } = ctx.request.userInfo;
        const { id } = ctx.params;
        ctx.validate(add_bill);
        const body = ctx.request.body;
        let { pay_type, amount, type_id, remark = '', createdTime } = body;
        const { name: type_name, url: type_url } = await ctx.service.type.getDetails(type_id);
        if (createdTime) createdTime = format('YYYY-MM-DD', createdTime);
        const updateTime = this.timeStamp();
        await ctx.service.bill.update({ id, user_id, pay_type, amount, type_id, type_name, type_url, remark, updateTime, createdTime });
        this.successful();
    }

    // 删除账单
    async delete() {
        const { ctx, app } = this;
        ctx.validate(delete_bill, ctx.params);
        // 把对应数据的actvie变成0，表示删除
        const { id: user_id } = ctx.request.userInfo;
        const { id } = ctx.params;
        await ctx.service.bill.delete(user_id, id);
        this.successful();
    }

    // 账单详情
    async details() {
        const { ctx, app } = this;
        ctx.validate(delete_bill, ctx.params);
        const { id: user_id } = ctx.request.userInfo;
        const { id } = ctx.params;
        const result = await this.service.bill.details(id, user_id);
        this.successful(result);
    }

    async list() {
        const { ctx } = this;
        ctx.validate(listQuery, ctx.query);
        const { id: user_id } = ctx.request.userInfo;
        const { page, size } = ctx.query;
        let query = { ...ctx.query };
        delete query.page;
        delete query.size;
        // 如果有pay_type过滤且pay_type为空则删除这个属性
        if (('pay_type' in query) && !query.pay_type) delete query.pay_type;
        // 格式化时间
        if (('createdTime' in query) && !query.createdTime) delete query.createdTime;
        else query.createdTime = format('YYYY-MM', query.createdTime);
        query = { ...query, user_id, active: 1 };
        // console.log(query);
        const list = await ctx.service.bill.list(query);
        if (!list.length) return this.successful({ content: [], pagination: { page, size, total: 0, last: 0 } });

        // 重组数据结构
        const newList = list.reduce((pre, cur) => {
            const date = cur.createdTime;
            //
            const index = pre.findIndex(i => i.date === date);
            // 如果在新数组里面能找到这个日期，就把当前cur的账单信息加入到这个日期里面去
            if (pre.length && index > -1) pre[index].bills.push(cur);
            // 如果找不到,就新建一项
            if ((pre.length && index === -1) || !pre.length) pre.push({ date, bills: [cur] });
            return pre;
        }, [])

        // 排序 => date倒叙，时间越新的在上面
        newList.sort((a, b) => a.date - b.date);

        // 分页
        const content = newList.slice((page - 1) * size, page * size);

        // 计算当前条件下的总支出/总收入
        const totalPrice = list.reduce((pre, cur) => {
            return add(pre, cur.amount);
        }, 0);

        // 获取分页详情数据
        const pagination = ctx.service.public.getPage(list.length);

        this.successful({ content, pagination, totalPrice });
    }
}
module.exports = BillController;
