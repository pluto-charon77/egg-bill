const PublicController = require('./public');

const fs = require('fs');
const path = require('path');
const format = require('../util/TimeFormats');
const mkdirp = require('mkdirp')

class UploadController extends PublicController {
    async upload() {
        const { app, ctx } = this;
        // 需要前往 config/config.default.js 设置 config.multipart 的 mode 属性为 file
        let file = ctx.request.files[0];
        // 声明存放资源的路径
        let uploadDir = '';

        try {
            let f = fs.readFileSync(file.filepath);
            let day = format('YYYYMMDD', new Date().getTime());
            // 创建图片保存的路径
            let dir = path.join(this.config.uploadDir, day);
            let date = new Date().getTime();
            await mkdirp(dir); // 创建对应目录
            // 返回图片保存路径
            uploadDir = path.join(dir, date + path.extname(file.filename));
            // 写入文件夹
            fs.writeFileSync(uploadDir, f);
        } finally {
            // 清除临时文件
            ctx.cleanupRequestFiles();
        }
        this.successful(uploadDir.replace(/app/g, ''));
    }
}

module.exports = UploadController;
