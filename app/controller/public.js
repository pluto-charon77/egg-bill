const Controller =  require('egg').Controller;
const HttpException = require('../exception/http');

// 自定义基类，封装常用方法,让其他controller去继承这个公共类，达到使用这些公共方法的目的
class PublicController extends Controller {
    // 成功返回的方法
    successful(data = '',code = 0, status = 'success', message = '') {
        this.ctx.body = {
            status,
            data,
            code,
            message,
        }
        this.ctx.status = 200;
    }
    // 抛出错误
    failure(message = '', code = 400, status = 'error', data= null) {
        throw new HttpException(message, code, status, data);
    }
    // 未找到的方法
    noFound(message = '资源不存在', code = 404, status = 'error', data = null) {
        throw new HttpException(message, code, status, data);
    }
    // 获取当前系统时间的时间戳(10位)
    timeStamp() {
        return Math.floor(new Date().getTime() / 1000);
    }
}

module.exports = PublicController;
