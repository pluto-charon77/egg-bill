const PublicController = require('./public');

const { typeMonth, typeYear, yearRule } = require('../validate/chart');

const { add, div, minus } = require('../util/math');

class ChartController extends PublicController {
    params = ['id', 'pay_type', 'amount', 'createdTime', 'updateTime', 'type_id', 'type_name', 'user_id', 'remark']

    // 标签/按月
    async typeMonth() {
        const { ctx, app } = this;
        ctx.validate(typeMonth, ctx.query);

        const { id: user_id } = ctx.request.userInfo;
        const { month, pay_type } = ctx.query;
        const like = `${month}%`
        const sql = `select ${this.params.join(',')} from bill where user_id=? and pay_type=? and active=1 and createdTime like ?`;
        const list = await ctx.service.chart.getBill(sql,[user_id,pay_type,like]);
        if(!list.length) return this.successful(null);

        const total = list.reduce((pre, cur) => {
            return add(pre, cur.amount);
        },0);
        const data = list.reduce((pre, cur) => {
            const index = pre.length ? pre.findIndex(i => i.type_id === cur.type_id) : -1;
            if(index === -1) {
                // 创建新类目
                const { type_id, type_name, pay_type, amount } = cur;
                const proportion = div(amount, total, 4); // 求该类目占总消费的比重，保留四位小数
                pre.push({
                    type_id,
                    type_name,
                    pay_type,
                    amount,
                    proportion
                });
            } else {
                // 已存在类目，对amount累加, 更新占比
                const { amount } = cur;
                pre[index].amount = add(pre[index].amount, amount);
                pre[index].proportion = div(pre[index].amount, total, 4);
            }
            return pre;
        }, []);
        this.successful({ total, date: month, data });
    }

    // 标签/年
    async typeYear() {
        const { ctx } = this;
        ctx.validate(typeYear, ctx.query);

        const { id: user_id } = ctx.request.userInfo;
        const { year, pay_type } = ctx.query;
        const sql = `select ${this.params.join(',')} from bill where user_id=? and active=1 and pay_type=? and createdTime like ?`;
        const like = `${year}%`;
        const list = await ctx.service.chart.getBill(sql, [user_id,pay_type,like]);
        if(!list.length) return this.successful(null);

        const total = list.reduce((pre, cur) => {
            return add(pre, cur.amount);
        }, 0);
        const data = list.reduce((pre, cur) => {
            const index = pre.length ? pre.findIndex(i => i.type_id === cur.type_id) : -1;
            if(index === -1) {
                const { type_id, type_name, pay_type, amount } = cur;
                const proportion = div(amount, total, 4); // 求该类目占总消费的比重，保留四位小数
                pre.push({
                    type_id,
                    type_name,
                    pay_type,
                    amount,
                    proportion
                });
            } else {
                // 已存在类目，对amount累加, 更新占比
                const { amount } = cur;
                pre[index].amount = add(pre[index].amount, amount);
                pre[index].proportion = div(pre[index].amount, total, 4);
            }
            return pre;
        }, []);
        this.successful({ total, date: year, data });
    }

    // 按年 => 查看每年各个月份的收支亲情况和结余
    async year() {
        const { ctx } = this;
        ctx.validate(yearRule, ctx.query);

        const { id: user_id } = ctx.request.userInfo;
        const { year } = ctx.query;
        const sql = `select ${this.params.join(',')} from bill where user_id=? and active=1 and createdTime like ?`;
        const like = `${year}%`;
        const list = await ctx.service.chart.getBill(sql, [user_id, like]);
        if(!list.length) return this.successful(null);

        let spendTotal = 0; // 支出总金额
        let incomeTotal = 0; // 收入总金额
        const data = list.reduce((pre,cur) => {
            const month = cur.createdTime.split('-')[1];
            const index = pre.length ? pre.findIndex(i => i.month === month) : -1;
            if(cur.pay_type === 1) spendTotal = add(cur.amount, spendTotal);
            if(cur.pay_type === 2) incomeTotal = add(cur.amount, incomeTotal);
            if(index === -1) {
                const send = cur.pay_type === 1 ? add(cur.amount, 0) : 0; // 支出
                const income = cur.pay_type === 2 ? add(cur.amount, 0) : 0; // 收入
                const balance =  minus(income, send);
                pre.push({
                    send,
                    income,
                    balance,
                    month,
                })
            } else {
                if(cur.pay_type === 1) pre[index].send = add(pre[index].send, cur.amount);
                if(cur.pay_type === 2) pre[index].income = add(pre[index].income, cur.amount);
                pre[index].balance = minus(pre[index].income, pre[index].send);
            }
            return pre;
        }, [])
        this.successful({
            year,
            spendTotal,
            incomeTotal,
            data,
        });
    }
}

module.exports = ChartController;
