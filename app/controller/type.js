const PublicController = require('./public');

class TypeController extends PublicController {
    async list() {
        const { ctx } = this;
        const res = await ctx.service.type.list();
        this.successful(res);
    }
}
module.exports = TypeController;
