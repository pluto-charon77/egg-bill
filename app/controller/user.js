const PublicController = require('./public');

const userRule = require('../validate/user');

const { hash, compare } = require('../util/bcrypt');

class UserController extends PublicController {
    // 用户注册
    async register() {
        const { ctx, app } = this;
        // 参数校验
        const body = ctx.request.body;
        ctx.validate(userRule);
        let { username, password } = body;
        const result = await ctx.service.user.isHas(username);
        if(result) return this.failure('用户已存在');
        const createdTime = this.timeStamp();
        // 加密
        password = await hash(password);
        const res = await ctx.service.user.register({ username, password, createdTime });
        if(res) return this.successful();
        this.failure('注册失败');
    }

    // 用户登录
    async login() {
        const { ctx, app } = this;
        const body = ctx.request.body;
        ctx.validate(userRule);
        const { username, password } = body;
        // 根据用户名查找用户信息
        const userInfo = await ctx.service.user.getUserInfo(username);
        if(!userInfo) return this.failure('用户不存在');
        const flag = await compare(password, userInfo.password);
        if(!flag) return this.failure('密码错误');
        // 生成token
        // app.jwt.sign 参数一为对象，是需要加密的内容, 参数二是jwt的加密字符串
        let token = app.jwt.sign({
            id: userInfo.id,
            username: userInfo.username,
            exp: Math.floor(Date.now() / 1000) + ( 24 * 60 * 60 ), // token有效期为24小时
        }, app.config.jwt.secret);
        token = `Bearer ${token}`;
        const data = { ...userInfo, ... { password: '' }, token };
        this.successful(data);
    }
}
module.exports = UserController;
