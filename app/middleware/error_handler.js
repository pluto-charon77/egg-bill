// app/middleware/error_handler.js
const HttpException = require('../exception/http');

module.exports = () => {
    return async function errorHandler(ctx, next) {
      try {
        await next();
      } catch (err) {
        // 所有的异常都在app上触发一个error事件，框架会记录一条错误日志
        ctx.app.emit('error', err, ctx);
        // console.log('erro===========>', err);

        let error = {};

        // 定义http的状态码
        let status = 200;
        // 判断是不是参数校验报错
        if(err.code === 'invalid_param') {
            // console.log('这是参数没有通过校验=>');
            status = 200;
            error.status = 'error';
            error.data = null;
            const arr = err.errors[0];
            error.message = `${arr.field} ${arr.message}`;
            error.code = 400;
        } else if(err instanceof HttpException) {
            // 判断异常是不是自定义异常
            // 自定义的一次状态码都是200
            status = 200;
            // ctx.status = err.httpCode;  改变http请求的状态码
            error.status = err.status;
            error.data = err.data;
            error.message = err.message;
            error.code = err.code;
        } else if(err.status === 500 && ctx.app.config.env === 'prod') {
            // 判断是不是服务器问题(线上环境)
            error = {
                status: 'Internal Server Error',
                message: err.message,
            };
            // status = 500;
        } else {
            error = {
                status: 'error',
                data: null,
                message: err.message,
                code: 0,
            }
        }
        ctx.status = status;
        ctx.body = error;
      }
    };
  };
