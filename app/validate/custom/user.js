module.exports = app => {
    let { validator } = app;

    // 使用validator.addRule(验证规则名称, (rule, value) => {})
    /**
     * rule
     * value 当前校验的值
     */
    // 用户名
    validator.addRule('userName', (rule, value) => {
        if(value.length < 1 || value > 10) return '用户名的长度应该在1-10之间';
    })

    // 十位的时间戳
    validator.addRule('timeStamp', (rule, value) => {
        let newValue = typeof value === 'number' ? value.toString() : value;
        if(newValue.length < 9 || newValue.length > 10) return '十位时间戳格式不正确';
    })
}
