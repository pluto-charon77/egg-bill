module.exports = app => {
    let { validator } = app;

    validator.addRule('typeMonth', (rule, value) => {
        if(!(/^\d{4}-((0([1-9]))|(1(0|1|2)))$/.test(value))) return '日期格式不正确';
    })

    validator.addRule('year', (rule, value) => {
        if(!(/^\d{4}$/).test(value)) return '年份格式不正确';
    })

    validator.addRule('dateFormat', (rule, value) => {
        if(!(/^((?:19|20)\d\d)-((0[1-9])|10|11|12)-((0[1-9])|(((1|2)[0-9]))|(30|31))$/.test(value))) return 'YYYY-MM-DD';
    })
}
