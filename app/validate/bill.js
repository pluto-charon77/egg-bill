module.exports = {
    add_bill: {
        pay_type: [1, 2], // 账单类型二选一
        // 金额最低为0
        amount: {
            type: 'number',
            min: 0,
        },
        type_id: 'number',
        // 备注为string类型，并且非必填
        remark: 'string?',

        // createdTime: 'dateFormat',
        createdTime: 'number',
        // remark: {
        //     type: 'string',
        //     required: false,
        //     allowEmpty: true, // 允许为空
        // }  // 这条等同于'string?'
    },
    // 删除账单
    delete_bill: {
        id: 'number',
    },
    // 分页过滤条件 => 创建时间和类型(支出/收入),page,size
    listQuery: {
        // 不存在或者为空字符串表示所以类型,1支出，2收入
        pay_type: {
            type: 'enum',
            values: ['', '1', '2'],
            required: true,
        },
        page: {
            type: 'number',
            required: true,
            default: 1, // 当属性为非必填，且该属性未填写时，使用1作为默认值
        },
        size: {
            type: 'number',
            required: true,
            default: 10,
        },
        createdTime: {
            type: 'timeStamp',
            required: true,
        },
    }
}
