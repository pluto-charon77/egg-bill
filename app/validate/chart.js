module.exports = {
    // 以标签按月查看 传递格式，形如YYYY-MM
    typeMonth: {
        month: 'typeMonth',
        pay_type: ['1', '2'],
    },
    typeYear: {
        year: 'year',
        pay_type: ['1', '2'],
    },
    yearRule: {
        year: 'year',
    }
}
