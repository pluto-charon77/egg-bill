'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  require('./router/user')(app);
  require('./router/bill')(app);
  require('./router/chart')(app);
  require('./router/upload')(app);
  require('./router/type')(app);
};
