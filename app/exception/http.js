class HttpException extends Error {
    constructor(message = '服务器异常',code = 400, status = 'error', data = null ) {
        super();
        this.message = message; // 自定义返回消息
        this.code = code; // 自定义状态码
        this.status = status; // 自定义状态
        this.data = data; // 自定义数据
    }
}

module.exports = HttpException;
