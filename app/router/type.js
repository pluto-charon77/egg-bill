module.exports = app => {
    const { router, controller } = app;

    // 获取账单标签列表
    router.get('/api/type/list', controller.type.list);
}
