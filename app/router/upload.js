module.exports = app => {
    const { controller, router } = app;

    // 上传文件接口
    router.post('/api/upload', controller.upload.upload);
}
