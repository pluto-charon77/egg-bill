
module.exports = (app) => {
    const { controller, router } = app;


    // 账单列表
    router.get('/api/bill/page', controller.bill.list);

    // 新增账单
    router.post('/api/bill/add', controller.bill.add);

    // 更新账单
    router.put('/api/bill/:id', controller.bill.update);

    // 删除账单
    router.delete('/api/bill/:id', controller.bill.delete);

    // 账单详情
    router.get('/api/bill/:id', controller.bill.details);
};
