
module.exports = (app) => {
    const { controller, router } = app;
    // 用户注册
    router.post('/api/user/register', controller.user.register);
    // 用户登录
    router.post('/api/user/login', controller.user.login);
  };
