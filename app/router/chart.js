module.exports = app => {
    const { controller, router } = app;

    // 以标签划分查看收支情况 => 按月
    router.get('/api/chart/type/month', controller.chart.typeMonth);

    // 以标签划分查看收支情况 => 按年
    router.get('/api/chart/type/year', controller.chart.typeYear)

    // 按年查看各个月的收支，结余
    router.get('/api/chart/year', controller.chart.year);
}
