const Big = require('big.js');

// 加法
const add = (num1, num2, fixed = 2) => (Big(num1).plus(num2)).toFixed(fixed);

// 减法
const minus = (num1, num2, fixed = 2) => (Big(num1).minus(num2)).toFixed(fixed);

// 求余（模） %
const mod = (num1, num2, fixed = 2) => (Big(num1).mod(num2)).toFixed(fixed);

// 除 /
const div = (num1, num2, fixed = 2) => (Big(num1).div(num2)).toFixed(fixed);

// 乘 *
const times = (num1, num2, fixed = 2) => (Big(num1).times(num2)).toFixed(fixed);

module.exports = {
    add,
    minus,
    mod,
    times,
    div,
}
