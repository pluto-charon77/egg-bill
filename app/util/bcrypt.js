const bcrypt = require('bcrypt');

// 加密
/**
 *
 * @param {*} str 需要加密的明文
 * @param {*} level 加密等级，等级越高，加密越复杂，默认为10
 * @returns 返回加密之后的密文
 */
const hash = (str, level = 10) => {
    return new Promise((resolve, reject) => {
        bcrypt.hash(str, level, (err, res) => {
            if(err) return reject(err);
            resolve(res);
        })
    })
}

// 对比明文和存储的密文
/**
 *
 * @param {*} str 需要加密的明文
 * @param {*} hash 明文加密完生成的密文
 * @returns 返回一个布尔值，为true表示该明文与密文来源一致
 */
const compare = (str, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(str, hash, (err, res) => {
            if(err) return reject(err);
            resolve(res);
        })
    })
}

module.exports = { hash, compare };
