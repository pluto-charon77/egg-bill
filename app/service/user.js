const Service = require('egg').Service;

class UserService extends Service {
    // 用于判断，用户是否已注册，用户名为唯一标识
    async isHas(username) {
        const { app } = this;
        const result = await app.mysql.get('user', { username });
        return result;
    }

    // 用户注册
    async register(data) {
        const { app } = this;
        const result = await app.mysql.insert('user', data);
        return result;
    }

    // 获取用户信息
    async getUserInfo(username) {
        const { app } = this;
        const result = await app.mysql.get('user', { username });
        return result;
    }

    // 用户登录
    async login(username) {
        const { app } = this;
    }
}

module.exports = UserService;
