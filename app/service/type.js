const Service = require('egg').Service;

class TypeService extends Service {
    // 获取标签详情
    /**
     *
     * @param {*} id 标签id
     * @returns
     */
    async getDetails(id) {
        const { app } = this;
        const result = await app.mysql.get('type', { id });
        return result;
    }

    // 获取标签列表
    async list() {
        const { app } = this;
        return await app.mysql.select('type');
    }
}

module.exports = TypeService;
