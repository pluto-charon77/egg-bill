const PublicService = require('./public');

class BillService extends PublicService {
    // 账单查询字段
    field = ['id', 'pay_type', 'amount', 'createdTime', 'updateTime', 'type_id', 'type_name', 'type_url', 'user_id', 'remark']

    // 账单详情
    async details(id, user_id) {
        const query = this.addActive({ id, user_id });
        let result = await this.getDetails('bill', {
            where: query, // where条件
            columns: this.field, // 要查询的字段
            orders: [['createdTime', 'desc'], ['id', 'desc']], // 排序方式
        });
        return result;
    }

    /**
     *
     * @param {*} params 插入账单的数据
     * @returns
     */
    async add(params) {
        const { app } = this;
        const result = await app.mysql.insert('bill', params);
        return result;
    }

    /**
     *
     * @param {*} params  修改的数据
     * @returns
     */
    async update(params) {
        const { app } = this;
        // id和user_id是过滤条件
        const { id, user_id } = params;
        const result = await app.mysql.update('bill', params, { id, user_id });
        return result;
    }

    // 删除账单
    async delete(user_id, id) {
        return await this.updateActive('bill', user_id, id);
    }

    // 账单列表
    async list(query) {
        const { app } = this;
        // return await app.mysql.select('bill', {
        //     where: query,
        //     columns: this.field,
        // })
        const { pay_type, createdTime } = query;
        const like = `${createdTime}%`;
        const sql = `select ${this.field} from bill where active = 1 and pay_type = ? and createdTime like ?`;
        const res = await app.mysql.query(sql, [pay_type, like]);
        return res;
    }
}

module.exports = BillService;
