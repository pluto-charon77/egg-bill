// Service的基类，包含常用的变量和方法

const Service = require('egg').Service;
const HttpException = require('../exception/http');

class PublicService extends Service {
    // 过滤条件，增加未删除(active=1)
    addActive(query) {
        return { ...query, active: 1};
    }

    // 获取分页参数
    pageParams() {
        const { page } = this.getCurrentParams(); // 页数
        const { size } = this.getCurrentParams();; // 一页显示多少条数据
        return {
            offset: (page - 1) * size,
            page,
            size,
        };
    }

    // 获取当前页数和当前一页显示多少条数据
    getCurrentParams(page = 1, size =10) {
        const { ctx } = this;
        const params = ctx.query;
        if(params.page && parseInt(params.page) > 0) {
            page = parseInt(params.page);
        }
        if(params.size && parseInt(params.size) > 0) {
            size = parseInt(params.size);
        }
        return { page, size };
    }

    /**
     * 获取分页的具体详情
     * @param {*} total  总数量
     * @returns
     */
    getPage(total) {
        const { page, size } = this.getCurrentParams();
        // 总数量不能整除size时，页数+1
        const last_page = total % size > 0 ? parseInt(total / size) + 1 : parseInt(total / size);
        return {
            total, // 数据总条数
            page, // 当前页数
            size, // 当前一页显示多少条数据
            last_page, // 最后一页的页数
        }
    }

    /**
     * 查看详情
     * @param {*} table 表名
     * @param {*} query 查看详情过滤的字段，是一个对象
     * @returns
     */
    async getDetails(table, query) {
        const { app } = this;
        // limit: 1, 返回数据量 offset: 0, 数据偏移量 => 返回一条数据u
        const res = await app.mysql.select(table, { ...query, ...{ limit:1, offset: 0 } });
        return res;
    }
    /**
     * 把改数据的active变成0就是删除
     * @param {*} table 表名
     * @param {*} user_id 用户id
     * @param {*} id 要删除的行数据的id
     * @returns
     */
    async updateActive(table,user_id,id) {
        const { app } = this;
        const query = { user_id, id };
        // 获取删除行数据的详情
        const res = await this.getDetails(table, { where: query });
        if(!res) throw new HttpException('数据不存在'); // 当未查到该数据时
        // 把active变成0表示删除
        const result = await app.mysql.update(table, { active: 0 }, { where: query });
        return result;
    }

}

module.exports = PublicService;
