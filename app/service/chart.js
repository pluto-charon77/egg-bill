const PublicService = require('./bill');

class ChartService extends PublicService {

    // 标签月查
    async getBill(sql,value) {
        const { app } = this;
        return await app.mysql.query(sql,value);
    }
}

module.exports = ChartService;
