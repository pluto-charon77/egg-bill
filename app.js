'use strict'

const path = require('path');

// 使用加载器，自动加载自定义校验规则
module.exports = app => {
    // 加载validate文件夹里面的文件
    // app.config.baseDir => 应用程序的当前目前，即根目录(package.json所在目录)
    // path.join()将多个参数字符串合并成一个路径字符串
    const directory = path.join(app.config.baseDir, 'app/validate/custom');
    // loadToApp用于加载一个目录到app
    app.loader.loadToApp(directory, 'validate');
}

class AppBootHook {
    constructor(app) {
      this.app = app;
    }

    // config 文件已经被读取并合并，但是还并未生效，这是应用层修改配置的最后时机
    configWillLoad() {
    }

    // 应用启动成功的生命周期
    async didReady() {
        const ctx = await this.app.createAnonymousContext();

        // 封装统一的调用检查函数，可以在查询、创建和更新等 Service 中复用
        ctx.service.checkSuccess = (result) => {
            if (result.status !== 200) {
                const errorMsg =
                  result.data && result.data.error_msg
                    ? result.data.error_msg
                    : 'unknown error';
                this.ctx.throw(result.status, errorMsg);
            }
            if (!result.data.success) {
              // 远程调用返回格式错误
              this.ctx.throw(500, 'remote response error', { data: result.data });
            }
        }
    }
}

// module.exports = AppBootHook;
